# Open API Demo

Sample Application to Demo Open API with Express Application.

## Installation

To install all dependencies

```shell
yarn
```

## Running Locally

To run the app locally

```shell
yarn start
```

Swagger Docs will be accessible at [api-docs](http://localhost:3000/api-docs/)

## Setup

### API Specs

Open API specs are available at `config/openapi.yml`

### Services

All business logic is present under `src/services/` folder

### Controller

All route handling logic is present under `src/api/controllers/` folder

### Express Server

All required configuration is managed under `src/utils/server.ts` file
