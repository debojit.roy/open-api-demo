/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import * as OpenApiValidator from 'express-openapi-validator';
import { Express } from 'express-serve-static-core';
import { connector, summarise } from 'swagger-routes-express';
import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs';

import * as api from '../api/controllers';

export async function createServer(): Promise<Express> {
  const yamlSpecFile = './config/openapi.yml'; // Get the Open API Spec
  const apiDefinition = YAML.load(yamlSpecFile); // Load the YAML Definition
  const apiSummary = summarise(apiDefinition); // Create Summary
  console.info(apiSummary);

  // Initialize Server
  const server = express();

  // setup API validator
  const validatorOptions = {
    apiSpec: yamlSpecFile,
    validateRequests: true,
    validateResponses: true,
  };

  // Add request body handlers
  server.use(express.json());
  server.use(express.text());
  server.use(express.urlencoded({ extended: false }));

  // Add Swagger UI endpoint
  server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(apiDefinition, { explorer: true }));

  // Add Open API validator
  server.use(OpenApiValidator.middleware(validatorOptions));

  // Handle Validation Errors
  server.use((err: any, _req: express.Request, res: express.Response, _next: express.NextFunction) => {
    console.error(err);
    res.status(err.status || 500).json({
      message: err.message,
      errors: err.errors,
    });
  });

  // Create the routes
  const connect = connector(api, apiDefinition, {
    onCreateRoute: (method: string, descriptor: any[]) => {
      console.log(`${method}: ${descriptor[0]} : ${(descriptor[1] as any).name}`);
    },
  });

  // Add routes to the server
  connect(server);

  return server;
}
