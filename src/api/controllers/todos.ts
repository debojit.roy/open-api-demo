import { Request, Response } from 'express';
import {
  getTodo as getTodoService,
  addTodo,
  updateTodo as updateTodoService,
  deleteTodo as deleteTodoService,
} from '../../services/TodoService';

/**
 *
 * @param req Express Request
 * @param res Express Response
 *
 * getTodo - Gets the requested to-do details based on id
 */
export const getTodo = (req: Request, res: Response): void => {
  const id = parseInt(req.params.id);
  const todos = getTodoService(id);

  if (todos.length > 0) {
    res.status(200).json(todos[0]);
  } else {
    res.status(404).json({ reason: 'Id not found' });
  }
};

/**
 *
 * @param req Express Request
 * @param res Express Response
 *
 * createTodo - Creates a new to-do
 */
export const createTodo = (req: Request, res: Response): void => {
  const message = req.body.message;
  const todoId = addTodo(message);
  res.status(201).json({ id: todoId, message });
};

/**
 *
 * @param req Express Request
 * @param res Express Response
 *
 * updateTodo - Updates an existing to-do with the new message
 */
export const updateTodo = (req: Request, res: Response): void => {
  const id = parseInt(req.params.id);
  const message = req.body.message;

  try {
    updateTodoService(id, message);
    res.status(200).end();
  } catch (err) {
    res.status(404).json({ reason: err.message });
  }
};

/**
 *
 * @param req Express Request
 * @param res Express Response
 *
 * deleteTodo - Deletes the requested to-do
 */
export const deleteTodo = (req: Request, res: Response): void => {
  const id = parseInt(req.params.id);

  try {
    deleteTodoService(id);
    res.status(200).end();
  } catch (err) {
    res.status(404).json({ reason: err.message });
  }
};
