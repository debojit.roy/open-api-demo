/**
 * Seed Data
 */
let todos = [
  { id: 0, message: 'First todo' },
  { id: 1, message: 'Second todo' },
];

/**
 * Id Tracker
 */
let currentTodoId = 2;

export interface ITodoService {
  getTodo: (id: number) => Array<{ id: number; message: string }>;
  addTodo: (message: string) => number;
  updateTodo: (id: number, message: string) => boolean;
  deleteTodo: (id: number) => boolean;
}

/**
 *
 * @param id Id of the todo item
 * @returns Array of todo items
 *
 * Searches and returns matching to-do
 */
export const getTodo = (id: number): Array<{ id: number; message: string }> => todos.filter(x => x.id === id);

/**
 *
 * @param message to-do message
 * @returns new to-do Id
 *
 * Adds a new to-do
 */
export const addTodo = (message: string): number => {
  const newTodoId = currentTodoId++;

  todos.push({
    id: newTodoId,
    message,
  });

  return newTodoId;
};

/**
 *
 * @param id to-do id
 * @param message new message
 * @returns operation success / failure
 *
 * Updates an existing to-do
 */
export const updateTodo = (id: number, message: string): boolean => {
  const todoList = getTodo(id);

  if (todoList.length === 0) {
    throw new Error('Invalid Todo Id');
  }

  todoList[0].message = message;

  return true;
}

/**
 *
 * @param id to-do id to delete
 * @returns operation success / failure
 *
 * Deletes an existing to-do
 */
export const deleteTodo = (id: number) : boolean => {
  const todoList = getTodo(id);

  if (todoList.length === 0) {
    throw new Error('Invalid Todo Id');
  }

  todos = todos.filter(x => x.id !== id);

  return true;
}
